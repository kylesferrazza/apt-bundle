use anyhow::Context;
use clap::Parser;

#[derive(Parser)]
#[clap(author, version, about)]
struct CliArgs {
    #[clap(help = "Name of the package to download")]
    pkg: String,

    #[clap(long, help = "ARCH")]
    arch: Option<String>,

    #[clap(long, help = "If not provided, defaults to <PKG>.tar.gz")]
    output_path: Option<std::path::PathBuf>,
}

fn main() {
    env_logger::builder().filter_module(std::module_path!(), log::LevelFilter::Info).init();
    if let Err(e) = apt_bundle() {
        log::error!("{}", e);
        std::process::exit(1);
    }
}

fn apt_bundle() -> anyhow::Result<()> {
    let args = CliArgs::parse();
    let tar_path = args
        .output_path
        .unwrap_or_else(|| (String::from(&args.pkg) + ".tar.gz").into());
    let output_result = std::process::Command::new("apt-rdepends").arg(&args.pkg).output();
    let output = output_result.context("Error calling apt-rdepends")?;
    let stdout = String::from_utf8(output.stdout).context("Error reading apt-rdepends stdout")?;
    let stderr = String::from_utf8(output.stderr).context("Error reading apt-rdepends stderr")?;
    if stderr.contains("Unable to locate package") {
        return Err(anyhow::anyhow!("apt-rdepends reports package does not exist"));
    }
    let mut deps = collect_dependencies(&stdout, &args.arch);
    log::info!("Got {} dependencies", deps.len());

    let temp_dir = tempdir::TempDir::new("download").context("Could not create temporary download directory")?;
    log::info!("Created temporary download directory at {}", &temp_dir.path().display());
    let temp_dir_path = temp_dir.path();
    let debs_dir_name = "debs";
    let debs_dir = temp_dir_path.join(debs_dir_name);
    std::fs::create_dir(&debs_dir).context(format!("Could not create debs directory: {}", &debs_dir.display()))?;

    let mut faulty_deps: Vec<String> = vec![];
    loop {
        let download_cmd_result = std::process::Command::new("apt-get")
            .arg("download")
            .args(&deps)
            .current_dir(&debs_dir)
            .output()
            .context("Error running apt-get download")?;
        if download_cmd_result.status.success() {
            log::info!("Downloaded {} packages and skipped {}", deps.len(), faulty_deps.len());
            break;
        }

        let stderr = String::from_utf8(download_cmd_result.stderr).context("Could not read stderr of the download process")?;
        let regexp = regex::Regex::new("Can't select candidate version from package (.*) as it has no candidate").context("Could not compile regex to match missing packages")?;
        let the_match = regexp.captures(&stderr).and_then(|captures| captures.get(1)).ok_or_else(|| anyhow::anyhow!("Could not determine the name of the faulty package"))?;
        let package_name = String::from(the_match.as_str());
        log::warn!("Could not download {}, ignoring it", &package_name);
        let index = deps.iter().position(|item| item == &package_name).ok_or_else(|| anyhow::anyhow!("Error determining faulty package name"))?;
        deps.remove(index);
        faulty_deps.push(package_name);
    }

    let tar_cmd_output = std::process::Command::new("tar")
        .arg("czvf")
        .arg(tar_path.as_os_str())
        .arg("-C")
        .arg(temp_dir_path)
        .arg(debs_dir_name)
        .output()
        .context("Error running tar")?;
    if !tar_cmd_output.status.success() {
        let status = match tar_cmd_output.status.code() {
            Some(code) => format!("{}", code),
            None => String::from("a non-zero exit code"),
        };
        return Err(anyhow::anyhow!(format!("Tar exited with {}", status)));
    }
    log::info!("Created tar file at path {}", &tar_path.display());

    temp_dir.close().context("Could not delete temporary directory")?;
    log::info!("Temporary download directory deleted");
    Ok(())
}

fn collect_dependencies(stdout: &str, arch: &Option<String>) -> Vec<String> {
    stdout
        .split('\n')
        .filter(|line| !line.starts_with(' ') && !line.is_empty())
        .map(|line| match arch {
            Some(a) => format!("{}:{}", line, a),
            None => line.to_string(),
        })
        .collect()
}
