{
  description = "apt-bundle";
  outputs = { self, nixpkgs }: let
    pkgs = import nixpkgs {
      system = "x86_64-linux";
    };
  in {
    defaultPackage.x86_64-linux = pkgs.rustPlatform.buildRustPackage rec {
      pname = "apt-bundle";
      version = "0.1.0";
      src = ./.;
      cargoSha256 = "sha256-Oq9dk/SkZsyuPw8rK7Lv2THBitFgTqB565tvCkSmlgA=";

      meta = with pkgs.lib; {
        description = "Downloads a dependency tree for a given apt package and creates a tarball of the result.";
        homepage = "https://gitlab.com/kylesferrazza/apt-bundle";
        license = licenses.mit;
        maintainers = [ maintainers.kylesferrazza ];
      };
    };
    devShell.x86_64-linux = pkgs.mkShell {
      name = "apt-bundle";
      buildInputs = with pkgs; [
        rustc
        cargo
        rust-analyzer
        clippy
        gcc
      ];
    };
  };
}
